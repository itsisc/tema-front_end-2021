 # **<Temă individuală>**
 ## **-Front-End-**

 Înainte de toate, dorim să te felicităm pentru parcursul tău de până acum din procesul de recrutare. Multă baftă la proiecte și de abia așteptăm să te revedem!

 ![](https://i.giphy.com/media/g9582DNuQppxC/giphy.webp)

 Cum s-ar spune, *"vorba multă, sărăcia omului..."*, aşa că să trecem direct la ce presupune tema individuală pentru divizia Front-End:

 Cu ajutorul cunoștințelor pe care le-ai dobândit la AC-uri va trebui să clonezi proiectul pe care tu vei lucra. În cadrul acestuia vei găsi   dosarul **assets/images** cu materiale necesare pentru realizarea temei. Ce vei avea de făcut este să realizezi un site funcțional ca și cel din videoclipul  **https://youtu.be/AfMkoxMZg84**

![](https://media.giphy.com/media/LmNwrBhejkK9EFP504/giphy.gif)

*   Pentru iconițe va trebui să folosești [**Font Awesome**](https://fontawesome.com/), o resursă online foarte utilă. Pentru a putea folosi Font Awesome, aveți nevoie de un link pe care să-l introduceți în codul vostru. Acest link îl găsiți pe următorul site: **https://cdnjs.com/**, căutând font awesome în search bar.
*   Pentru font-uri poți folosi [**Google Fonts**](https://fonts.google.com/), o altă resursă foarte utilă. Font-urile folosite pe site sunt **Mitr** pentru titluri și **Raleway** pentru conținut.

`++ Opțional!` După cum bine știi, un site nu este complet fără să fie responsive, așadar te încurajăm să încerci să adaptezi site-ul făcut de tine și să adăugi și varianta pentru mobile a acestuia. Acest task nu este obligatoriu, dar ne-ar plăcea să vedem cum te descurci!

![](https://i.pinimg.com/originals/28/6c/e0/286ce0190a188a8958bd81b5420eb09d.gif)

Sperăm să accepți cu entuziasm maxim ca până acum și această provocare. Nu uita, oricât de complicate ar părea lucrurile, încercarea moarte n-are și prietenul nostru [**Google**](https://www.google.com/) este mereu acolo. 

Așadar, pentru a trimite tema, va trebui să creezi o arhivă pe care să o denumești **TemaIT_FrontEnd_NumePrenume** și care să conțină tot ce ai făcut. 


![](https://media4.giphy.com/media/3o6Mbfsf4DI4Cds5Ms/giphy.gif)

Deadline-ul pentru a trimite tema pe mail este: `Joi, 4 Noiembrie, ora 23:59`

 **Succes!**
